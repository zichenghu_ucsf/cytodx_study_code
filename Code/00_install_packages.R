##### install packages from CRAN #####
pkg1 = c("dplyr", "tidyr", "ROCR", "ggplot2", "rpart", 
         "survival", "glmnet","devtools")
install.packages(pkg1)

##### install packages from bioconductor #####
pkg2 = c("cydar", "ncdfFlow", "edgeR" )
source("https://bioconductor.org/biocLite.R")
biocLite(pkg2)

##### install packages from github #####
library("devtools")
install_github("hzc363/CytoDx")
